import {DefaultCrudRepository} from '@loopback/repository';
import {Stuff} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class StuffRepository extends DefaultCrudRepository<
  Stuff,
  typeof Stuff.prototype.id
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Stuff, dataSource);
  }
}
