import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Stuff} from '../models';
import {StuffRepository} from '../repositories';

export class StuffController {
  constructor(
    @repository(StuffRepository)
    public stuffRepository : StuffRepository,
  ) {}

  @post('/stuffs', {
    responses: {
      '200': {
        description: 'Stuff model instance',
        content: {'application/json': {schema: {'x-ts-type': Stuff}}},
      },
    },
  })
  async create(@requestBody() stuff: Stuff): Promise<Stuff> {
    return await this.stuffRepository.create(stuff);
  }

  @get('/stuffs/count', {
    responses: {
      '200': {
        description: 'Stuff model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Stuff)) where?: Where,
  ): Promise<Count> {
    return await this.stuffRepository.count(where);
  }

  @get('/stuffs', {
    responses: {
      '200': {
        description: 'Array of Stuff model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': Stuff}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Stuff)) filter?: Filter,
  ): Promise<Stuff[]> {
    return await this.stuffRepository.find(filter);
  }

  @patch('/stuffs', {
    responses: {
      '200': {
        description: 'Stuff PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody() stuff: Stuff,
    @param.query.object('where', getWhereSchemaFor(Stuff)) where?: Where,
  ): Promise<Count> {
    return await this.stuffRepository.updateAll(stuff, where);
  }

  @get('/stuffs/{id}', {
    responses: {
      '200': {
        description: 'Stuff model instance',
        content: {'application/json': {schema: {'x-ts-type': Stuff}}},
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Stuff> {
    return await this.stuffRepository.findById(id);
  }

  @patch('/stuffs/{id}', {
    responses: {
      '204': {
        description: 'Stuff PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody() stuff: Stuff,
  ): Promise<void> {
    await this.stuffRepository.updateById(id, stuff);
  }

  @put('/stuffs/{id}', {
    responses: {
      '204': {
        description: 'Stuff PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() stuff: Stuff,
  ): Promise<void> {
    await this.stuffRepository.replaceById(id, stuff);
  }

  @del('/stuffs/{id}', {
    responses: {
      '204': {
        description: 'Stuff DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.stuffRepository.deleteById(id);
  }
}
