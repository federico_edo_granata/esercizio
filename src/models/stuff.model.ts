import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class Stuff extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;


  constructor(data?: Partial<Stuff>) {
    super(data);
  }
}
